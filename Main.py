import smtplib
import urllib.request
import time
import sys

def sendEmail(): 
    toAddress = 'toAddress@example.com'
    fromAddress = 'fromAddress@example.com'
    subject = 'Keyword match'
    password = 'enterPasswordHere'

    try:
        smtpserver = smtplib.SMTP("smtp.gmail.com",587)
        smtpserver.ehlo()
        smtpserver.starttls()
        smtpserver.login(fromAddress, password)
        header = 'To:' + toAddress + '\n' + 'From: ' + fromAddress + '\n' + 'Subject:' + subject + '\n'
        message = header + '\n There is an item on special. \n\n'
        smtpserver.sendmail(fromAddress, toAddress, message)
    except:
        print ("An error with the mail occured")
    finally:
        smtpserver.close()

def downloadWebPage():  
    path = 'C:/Users/Michael/Desktop/Python test/webPage.txt'
    url = 'enterURLOfWebsiteHere' 

    try:
        response = urllib.request.urlopen(url)
        data = response.read()
        text = data.decode('utf-8')

        file = open(path, 'w')
        file.write(text)
        file.close()
        file = open(path)

        return file
    except:
        print ("An error occured when downloading")

def parseHtml():
    result = "Not found"
    keyword = "enterKeywordHere"
    file = downloadWebPage()

    for line in file:
        if keyword in line:
            result = line
            print ("FOUND")
            sendEmail()
            sys.exit()
            

def main():
    print ("Starting script")
    global count
    count = 1
    
    while True:
        print ("Check #" + str(count))
        parseHtml()
        time.sleep(5)
        count+=1

main()