This script downloads and scans a website's HTML code for a certain key word that you specify.

For example, when The Warehouse has an item on special, they add a div tag with the class name "badgeA". When a video game comes on special you will be notified so you can purchase it before it sells out.